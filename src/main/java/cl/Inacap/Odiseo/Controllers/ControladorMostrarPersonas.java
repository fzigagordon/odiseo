package cl.Inacap.Odiseo.Controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.Odiseo.DAO.PersonaDAO;
import cl.Inacap.Odiseo.DTO.Persona;

@WebServlet(name = "ControladorMostrarPersonas.do", urlPatterns = { "/ControladorMostrarPersonas.do" })
public class ControladorMostrarPersonas extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ControladorMostrarPersonas() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonaDAO pDAO = new PersonaDAO();
		List<Persona> personaList = pDAO.getAllPersonas();
		request.setAttribute("ListaPersonas", personaList);
		request.getRequestDispatcher("site/mostrarPersonas.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
