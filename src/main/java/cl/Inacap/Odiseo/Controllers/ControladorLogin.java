package cl.Inacap.Odiseo.Controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.Inacap.Odiseo.DAO.PersonaDAO;
import cl.Inacap.Odiseo.DTO.Persona;

@WebServlet(name = "ControladorLogin.do", urlPatterns = { "/ControladorLogin.do" })
public class ControladorLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ControladorLogin() {
        super();
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("site/register.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ADD PERSONA
				String Nombre=request.getParameter("nombre").toString();	
				String Email=request.getParameter("Email").toString();	
				String Password=request.getParameter("Password").toString();	
				
				Persona p=new Persona();
				p.setNombre(Nombre);
				p.setEmail(Email);
				p.setPass(Password);
				
				PersonaDAO pdao=new PersonaDAO();
				pdao.AddPersona(p);
				
				response.sendRedirect("ControladorWeb.do");
				
				
				
				
				/*
				 * Persona p=new Persona(request.getParameter("nombre").toString(),request.getParameter("email").toString(),request.getParameter("pass").toString());
				PersonaDAO pDao=new PersonaDAO();
				pDao.AddPersona(p);
				
				Persona p= new Persona();
				p.setNombre(request.getParameter("nombre").toString());
				p.setEmail(request.getParameter("email").toString());
				p.setPass(request.getParameter("pass").toString());
				*/
	}

}
