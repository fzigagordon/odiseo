package cl.Inacap.Odiseo.DTO;

//import com.oracle.wls.shaded.org.apache.bcel.classfile.SourceFile;

public class Libro {

	private String nombre;
	private String autor;
	private String cantidadPaginas;
	private String booleanDestacado;
	private String portada;
	private String estrellas;
	private String categoria;
	
	public Libro(String nombre, String autor, String cantidadPaginas, String booleanDestacado,String portada, String estrellas,
			String categoria) {
		super();
		this.nombre = nombre;
		this.autor = autor;
		this.cantidadPaginas = cantidadPaginas;
		this.booleanDestacado = booleanDestacado;
		this.portada = portada;
		this.estrellas = estrellas;
		this.categoria = categoria;
	}

	public Libro() {}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getCantidadPaginas() {
		return cantidadPaginas;
	}

	public void setCantidadPaginas(String cantidadPaginas) {
		this.cantidadPaginas = cantidadPaginas;
	}

	public String BooleanDestacado() {
		return booleanDestacado;
	}

	public void setBooleanDestacado(String booleanDestacado) {
		this.booleanDestacado = booleanDestacado;
	}
	
	public String portada() {
		return portada;
	}

	public void setPortada(String portada) {
		this.portada = portada;
	}
	public String getEstrellas() {
		return estrellas;
	}

	public void setEstrellas(String estrellas) {
		this.estrellas = estrellas;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	
}
