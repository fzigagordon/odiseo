package cl.Inacap.Odiseo.DAO;

import java.util.ArrayList;
import java.util.List;

import cl.Inacap.Odiseo.DTO.Persona;

public class PersonaDAO {
	private static List<Persona> arrPersonas = new ArrayList<Persona>();
	
	public void AddPersona(Persona p) {
		arrPersonas.add(p);
	}
	
	public List<Persona> getAllPersonas(){
		return arrPersonas;
	}
	
	public void deletePersona(int id) {
		arrPersonas.remove(id);
	}
	
	public Persona getPersonabyID(int id) {
		return arrPersonas.get(id);
	} 
	public void updatePersona(Persona p,int id) {
		arrPersonas.set(id,p);
	}
}
