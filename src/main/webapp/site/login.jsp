<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Spartan:wght@300;600&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="site/css/style.css"/>

    <title>Odiseo | Login</title>

  </head>
  <body class="bg-dark">
      <section>
        <div class="row g-0">
            <div class="col-lg-7 d-none d-lg-block">
                <div id="carrucel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-target="#carrucel" data-slide-to="0" class="active"></li>
                      <li data-target="#carrucel" data-slide-to="1"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item img-1 min-vh-100 active">
                        <div class="carousel-caption d-none d-md-block">
                          <h5 class="font-weight-bold">¿Aún no te registras?</h5>
                          <a class="text-muted text-decoration-none">Registrate ahora!!</a>
                        </div>
                      </div>
                      <div class="carousel-item img-2 min-vh-100">
                        <div class="carousel-caption d-none d-md-block">
                          <h5 class="font-weight-bold">No sabes que leer</h5>
                          <a class="text-muted text-decoration-none">Visita nuestro catálogo</a>
                        </div>
                      </div>
                    </div>
                    <a class="carousel-control-prev" href="#carrucel" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carrucel" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
               </div>
            </div>
            <div class="col-lg-5 bg-dark d-flex flex-column align-items-end min-vh-100">
                <div class="px-lg-5 pt-lg-4 pb-lg-3 p-4 mx-auto centrar mb-auto w-100">
                    <img src="site/img/logo/logo.svg" class="img-fluid img-logo" />
                </div>
                <div class="align-self-center w-100 px-lg-5 py-lg-4 p-4">
                	<h1 class="font-weight-bold mb-4">Inicio De Sesión</h1>

                	<form action="ControladorLogin.do" method="POST" class="mb-5" name="formulario" id="formularioID">
                    	<div class="mb-4">
                      	<label for="email" class="form-label font-weight-bold">Email</label>
                      	<input type="email" class="form-control bg-dark-x border-0" placeholder="ingresa tu email@servidor.dominio" onchange="validarEmail(this.value)" aria-describedby="ayuda" id="email" name="email">
                    	</div>
	                    <div class="mb-4">
	                      <label for="pass" class="form-label font-weight-bold">Contraseña</label>
	                      <input type="password" class="form-control bg-dark-x border-0 mb-2" placeholder="Ingresa tu contraseña" name="pass" id="pass">
	                      <a href="#" id="ayuda" class="form-text text-muted text-decoration-none">¿Has olvidado tu contraseña?</a>
	                    </div>
                    	<button type="button" onclick="validarInfo()" class="btn btn-primary w-100" id="btn-send" value="enviar"name="btn-send" >Iniciar sesión</button>
                 	</form>

                	<p class="font-weight-bold text-center text-muted">O inicia sesión con</p>
	                <div class="d-flex justify-content-around">
	                    <button type="button" class="btn btn-outline-light flex-grow-1 mr-2"><i class="fab fa-google lead mr-2"></i> Google</button>
	                    <button type="button" class="btn btn-outline-light flex-grow-1 ml-2"><i class="fab fa-facebook-f lead mr-2"></i> Facebook</button>
	                </div>
                </div>
                <div class="text-center px-lg-5 pt-lg-3 pb-lg-4 p-4 mt-auto w-100">
                    <p class="d-inline-block mb-0">¿Todavía no tienes una cuenta?</p> <a href="" class="text-light font-weight-bold text-decoration-none">Crea una ahora</a>
                </div>
            </div>
        </div>
      </section>
   
  </body>
  
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/ffec4ec2ed.js" crossorigin="anonymous"></script>
    
    <script>
      $(document).ready(function () {});
      
    function validarInfo(){ //FUNCION PARA EL BOTON ONCLICK
        if( $("#email").val()=='' || $("#pass").val()==''){
            alert("Favor de ingresar ambos datos");
        return 0;
        }
        else{
        	$("#formularioID").submit();
        }
    //document.formulario.submit();
    }
    </script>
</html>